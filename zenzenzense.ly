\version "2.18.2"

\header {
	title = "Zenzenzense"
	tagline = "Transcribed by /u/Arandur"
}

\score {
	\new Staff {
		\relative b {
			\clef "treble_8"
			\key b \major
			\time 8/8
			\compressFullBarRests
			R1*7
			r2 r4. fis8
			b8. cis16 b4~b8. fis16 \tuplet 3/2 { e'8 dis cis } |
			b2 r4 b8 cis |
			dis16 cis dis e dis b b cis dis cis dis e dis8 cis16 b~ |
			b2 r4 r8. fis16 |
			b8. fis16 fis4~fis8. fis16 \tuplet 3/2 { e'8 dis cis } |
			b2 r4 b8 cis |
			dis16 cis dis e dis b b cis dis cis dis e dis8 cis16 cis(~ |
			cis b8.~b4) r4 r8 b16 cis |
			dis16 cis dis e dis b b cis dis cis dis e dis8 cis16 cis(~ |
			cis b8.~b4) r2 |

			b16 cis dis b cis dis b cis dis b cis dis b cis dis b |
			fis'4. cis16 cis16~cis4 r4 |
			b16 cis dis b cis dis b cis dis b cis dis b cis dis b |
			fis'4. cis16 cis16~cis4 r8 ais16 gis |
			ais8 gis16 ais8 gis16 b4( gis8) gis8. fis16 |
			cis'8 cis16 cis8 dis16 e dis16~dis8. r16 r8. gis,16 |
			ais8 gis16 ais8 gis16 b4 gis8 fis4 |
			dis'4~dis8. cis16 cis4~cis8. b16 |
			b4~b8. cis16 cis4 r16 b16 b b |

			fis'8 fis fis dis16 cis cis cis8 b b16 b b |
			fis'16 fis8 fis16 fis dis dis cis~cis8. fis,16 b cis dis( cis~ |
			cis) b b fis b cis dis cis~cis b b fis b cis b8 |
			fis' dis16 fis8 dis dis16( cis4) r16 b16 b b |
			fis'8 fis fis dis16 cis8 cis b b16 b b |
			fis'8 dis16 fis8 dis dis16( cis8.) b16~b cis dis cis~ |
			cis b b fis b cis dis cis~cis b b fis b cis dis cis~ |
			cis b b8~b4 r8 b16 fis b8 dis16 cis~ |
			cis b b fis b8 dis16 cis~cis b b fis b cis dis cis~ |
			cis b b8~b4 r2 |

			R1*4

			b8. fis16 fis4~fis8. fis16 \tuplet 3/2 { e'8 dis cis } |
			b2 r4 b8 cis |
			dis16 cis dis( e) dis b b cis dis cis dis e dis8 cis16 b~ |
			b2 r2 |
			b8. fis16 fis4~fis8 fis8 \tuplet 3/2 { e'8 dis cis } |
			b2 r4 b8( cis) |
			dis16 cis dis e dis b b cis dis cis dis e dis8 cis16 b~ |
			b2 r4 b8 cis |
			dis16 cis dis e dis b b cis dis cis dis e dis8 cis16 b~ |
			b2 r2 |

			b16 cis dis b cis dis b cis dis b cis dis b cis dis b |
			fis'4. cis16 cis16~cis4 r4 |
			b16 cis dis b cis dis b cis dis b cis dis b cis dis b |
			fis'4. cis16 cis16~cis4 r4 |
			ais8 gis16 ais8 gis16 b4 fis8 gis8. fis16 |
			cis'8 cis16 cis8 dis16 e dis16~dis8. r16 r8. gis,16 |
			ais8 gis16 ais8 gis16 b4 cis8 b4 |
			dis4~dis8. cis16 cis4~cis8. b16 |
			b4~b8. cis16 cis4 r16 b16 b b |

			fis'8 fis fis dis16 cis cis cis8 b b16 b b |
			fis'16 fis8 fis16 fis dis dis cis~cis8. fis,16 b cis dis cis~ |
			cis b b fis b cis dis cis~cis b b fis b cis b8 |
			fis' dis16 fis8 dis dis16( cis4) r16 b16 b b |
			fis'8 fis fis dis16 cis8 cis b b16 b b |
			fis' fis fis fis fis dis dis dis cis8. b16 b cis dis cis~ |
			cis b b fis b cis dis cis~cis b b fis b cis dis cis~ |
			cis b b8~b4 r8 b16 fis b8 dis16 cis~ |
			cis b b fis b8 dis16 cis~cis b b fis b cis dis cis~ |
			cis b b8~b4 r2 |

			R1*22

			fis'8 fis fis dis16 cis cis cis8 b b16 b b |
			fis'16 fis8 fis16 fis dis dis cis( b8.) fis16 b cis dis( cis~ |
			cis) b b fis b cis dis cis~cis b b fis b cis b8 |
			fis' dis16 fis8 dis dis16( cis4) r16 b16 b b |
			fis'8 fis fis dis16 cis8 cis b b16 b b |
			fis'8 dis16 fis8 dis dis16( cis8.) b16 b cis dis cis~ |
			cis b b fis b cis dis cis~cis b b fis b cis dis cis~ |
			cis b b8~b4 r8 fis8 b16 cis dis cis~ |
			cis b b fis b cis dis16 cis~(cis b) b fis b cis dis cis~ |
			cis b b8~b4 r2 |

			R1*6 \bar "|."
		}
		\addlyrics {
			Yat -- to me wo sa -- ma -- shi -- ta kai
			so -- re na no ni na -- ze me mo a -- wa -- se ya shi -- nai -- 'n dai?
			"\"O" -- soi "yo\"" to o -- ko -- ru ki -- mi
			ko -- re -- de -- mo ya -- re -- ru da -- ke to -- ba -- shi -- te ki -- tan -- da yo.

			Ko -- ko -- ro ga ka -- ra -- da wo o -- i -- ko -- shi -- te ki -- tan -- da yo.

			Ki -- mi no ka -- mi ya hi -- to -- mi da -- ke de mu -- ne ga i -- ta -- i yo
			O -- na -- ji to -- ki wo su -- i -- ko -- n -- de ha -- na -- shi -- ta -- kun -- a -- i yo
			Ha -- ru -- ka mu -- ka -- shi ka -- ra shi -- ru so -- no ko -- e ni
			U -- ma -- re -- te ha -- ji -- me -- te na -- ni wo i -- e -- ba ii?

			Ki -- mi no zen zen zen -- se ka -- ra bo -- ku~wa
			ki -- mi wo sa -- ga -- shi~ha -- ji -- me -- ta yo
			So -- no bu -- ki -- ccho na wa -- ra -- i -- ka -- ta wo me -- ga -- ke -- te
			ya -- tte ki -- tan -- da yo

			Ki -- mi ga zen -- zen zen -- bu na -- ku~na -- tte
			chi -- ri -- ji -- ri ni na -- tta -- tte
			Mou ma -- yo -- wa -- na -- i
			ma -- ta i -- chi ka -- ra sa -- ga -- shi ha -- ji -- me -- ru sa
			Mu -- shi -- ro ze -- ro ka -- ra
			ma -- ta u -- chuu wo ha -- ji -- me -- te mi -- yo -- u ka

			Do -- kka -- ra ha -- na -- su ka -- na
			ki -- mi ga ne -- mu -- tte -- i -- ta a -- i -- da no su -- too -- ri -- i
			Nan -- 'o -- ku nan -- ko -- u -- ne -- n -- bun no
			mo -- no -- ga -- ta -- ri wo ka -- ta -- ri ni ki -- tan -- da yo
			ke -- do i -- za so -- no su -- ga -- ta ko -- no me ni u -- tsu -- su to

			Ki -- mi mo shi -- ra -- nu ki -- mi to ja -- re -- te ta -- wa -- mu -- re -- ta -- i yo
			Ki -- mi no ki -- e -- nu i -- ta -- mi ma -- de a -- i -- shi -- te mi -- ta -- i yo
			Gin -- ga nan -- ko -- bun ka no ha -- te ni de -- a -- e -- ta
			So -- no te wo ko -- wa -- sa -- zu dou ni -- gi -- tta na -- ra ii?

			Ki -- mi no zen zen zen -- se ka -- ra bo -- ku~wa
			ki -- mi wo sa -- ga -- shi~ha -- ji -- me -- ta yo
			So -- no sa -- wa -- ga -- shi -- i ko -- e to na -- mi -- da wo me -- ga -- ke
			ya -- tte ki -- tan -- da yo

			So -- n -- na ka -- kumei zen -- ya no bo -- kura wo~da -- re ga 
			to -- me -- ru to i -- u'n da -- ro -- u
			Mo -- u ma -- yo -- wa -- na -- i ki -- mi no ha -- a -- to ni ha -- ta wo ta -- te -- ru yo
			Ki -- mi wa bo -- ku ka -- ra a -- ki -- ra -- me -- ka -- ta wo
			u -- ba -- i -- to -- tta no

			Zen zen zen -- se ka -- ra bo -- ku~wa
			ki -- mi wo sa -- ga -- shi~ha -- ji -- me -- ta yo
			So -- no bu -- ki -- ccho na wa -- ra -- i -- ka -- ta wo me -- ga -- ke -- te
			ya -- tte ki -- tan -- da yo

			Ki -- mi ga zen -- zen zen -- bu na -- ku~na -- tte
			chi -- ri -- ji -- ri ni na -- tta -- tte
			Mo -- u ma -- yo -- wa -- na -- i
			ma -- ta i -- chi ka -- ra sa -- ga -- shi ha -- ji -- me -- ru sa
			Na -- n -- ko -- u -- nen de -- mo ko -- no u -- ta wo ku -- chi -- zu -- sa -- mi na -- ga -- ra
		}
	}
}
